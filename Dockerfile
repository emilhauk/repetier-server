FROM ubuntu

RUN apt-get -qq update \
 && apt-get -qq install curl > /dev/null \
 && apt-get -qq clean

RUN curl http://download.repetier.com/files/server/debian-amd64/Repetier-Server-0.86.2-Linux.deb > repetier-server.deb \
 && dpkg --unpack repetier-server.deb \
 && rm -f repetier-server.deb \
 # We do this to avoid failing on service installation. 
 && rm -f /var/lib/dpkg/info/repetier-server.postinst \
 && apt-get -qqf install > /dev/null

RUN adduser --quiet --home /var/lib/Repetier-Server --system --disabled-login --ingroup dialout repetierserver \
 && adduser repetierserver tty

RUN rm -rf /var/lib/Repetier-Server \
 && ln -sf /data /var/lib/Repetier-Server

VOLUME "/data"

EXPOSE 3344

USER repetierserver

CMD [ "/usr/local/Repetier-Server/bin/RepetierServer", "-c", "/usr/local/Repetier-Server/etc/RepetierServer.xml" ]
