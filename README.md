# Repetier server

## Start manually:
`docker run --privileged --rm -p 80:3344 -v <repetier data folder/volume>:/data --name repetier emilhauk/repetier-server`

## Systemd service:
Write the following to `/etc/systemd/system/repetier-server.service`
```ini
[unit]
Description=Repetier Server
Requires=docker.service
After=docker.service

[Service]
Restart=always
ExecPreStart=-/usr/bin/docker stop %n
ExecPreStart=-/usr/bin/docker rm %n
ExecStart=/usr/bin/docker run --privileged --rm -p 80:3344 -v <repetier data folder/volume>:/data --name %n emilhauk/repetier-server:latest
ExecStop=/usr/bin/docker stop %n

[Install]
WantedBy=multi-user.target
```

Install service: `systemctl enable repetier-server.service`

## Alternative configuration
If you'd like to limit the access the container has on the system, you can skip the `--privileged`-flag and insted send in specific devices using `--device /dev/ttyACM0:/dev/fooBarPrinter`-flags
